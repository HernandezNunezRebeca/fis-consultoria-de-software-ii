1


# Nearsoft
Link: https://nearsoft.com/

Ubicación: https://goo.gl/maps/RLfEZCiT4hQWRB1W8

Acerca de: "Somos una comunidad de más de 300 personas en oficinas en todo México, y nos reunimos dos veces al año para nuestra Semana de Team Building. Durante esa semana trabajamos para mejorar nuestra relación laboral, definiendo nuestras metas y —por supuesto— nos divertimos mucho."

Presencia:
Facebook- https://www.facebook.com/Nearsoft/
twitter- https://twitter.com/Nearsoft


Ofertas laborales: https://nearsoft.com/join-us/

Blog de ingenieria: https://nearsoft.com/blog/

Tecnologías:
*Java
*Python
*Ruby
*Elixir
*Scala



2


# Michelada.io

Link: https://www.michelada.io/

Ubicación: https://goo.gl/maps/FcVndKG3mWVtM9Lq9

Servicios: "Desarrollamos software para soluciones interactivas y picantes. Ya sea que sea una empresa nueva o una corporación, podemos ayudarlo a transformar grandes ideas en software utilizable para productos atractivos."

Presencia:
Facebook- https://www.facebook.com/micheladaio
twitter- https://twitter.com/micheladaio

Ofertas laborales: https://jobs.michelada.io/

Blog de ingenieria: https://github.com/michelada


3


# MagmaLabs

Link: https://www.magmalabs.io/

Ubicación: https://goo.gl/maps/uTjuwVra5tdBj6j38

Acerca de: "Somos una agencia de desarrollo de software estadounidense con operaciones en México y otras partes de América Latina. Desde el inicio, nuestra misión ha sido impulsar la innovación mientras desarrollamos talento humano de clase mundial."

Servicios: 
Ingenieria
Diseño

Presencia:
Fcaebook- https://www.facebook.com/magmalabsio
Twitter- https://twitter.com/weareMagmaLabs
Instagram- https://www.instagram.com/magmalabs/

Ofertas laborales: https://magmalabs.bamboohr.com/jobs/

Blog de ingenieria: http://blog.magmalabs.io/

Tecnologias: 
*GoPro
*Minimal Audio
*Optum
*Arca

4


# Density Labs

Link: https://densitylabs.io/

Ubicación: https://goo.gl/maps/jb1bWG96e1hwhhnc6

Acerca de: "Density Labs es una empresa de desarrollo de software que presta servicios a las organizaciones para acelerar el desarrollo de aplicaciones web, móviles y SaaS. Escale rápidamente su equipo de desarrollo, reduzca el tiempo de comercialización y aproveche la economía de costos del aumento de personal local."

Servicios:
Desarrolladores de software
Control de calidad y automatización
DevOps
Gestión de proyectos
Diseño UX / UI

Presencia:
Twitter- https://twitter.com/densitylabs
Facebook- https://www.facebook.com/densitylabs
Youtube- https://www.youtube.com/channel/UCk5QUrs2j63QpC8JjQ8yr-A

Ofertas laborales: https://densitylabs.io/careers

Blog de ingenieria: https://densitylabs.io/blog

Tecnologias:
*React native
*Ember
*Rails



5


# iTexico

Link: https://www.itexico.com/

Ubicación: https://g.page/improving-guadalajara?share

Acerca de: "La mejora ha ayudado a más de 300 clientes a reducir los riesgos de subcontratar el desarrollo de software aprovechando un modelo de negocio Nearshore +, integrando las mejores prácticas de los equipos ágiles en la forma en que necesita la innovación digital e implementando las mejores prácticas de prestación de servicios empresariales."

Servicios: 
Diseño UI / UX
Servicios de desarrollo de software
QA
Móvil
.NETO
Inteligencia artificial
Nube
Java
JavaScript
DevOps

Presencia:
https://twitter.com/improvingmx
https://www.facebook.com/itexico
https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw

Ofertas laborales: https://www.itexico.com/careers

Blog de ingenieria: https://www.itexico.com/blog

Tecnologias:
*Iridium
*Integral ad science
*Western digita




6

# TangoSource

Link: https://tango.io/

Ubicación: https://goo.gl/maps/PKKVMtnNHmo9hhbm8

Acerca de: Tango (anteriormente TangoSource), ayuda a empresas emergentes, medianas y empresariales innovadoras a desarrollar productos digitales impactantes a través de asociaciones apasionadas. ¡Hemos ayudado a más de 100 empresas a desarrollar y escalar sus productos de software desde 2009! "

Servicios:
DevOps
WebDev
Móvil
SaaS

Presencia:
https://www.instagram.com/tangodotio/
https://twitter.com/tango_io
https://www.facebook.com/Tango.ioMX

Ofertas laborales: https://jobs.lever.co/tango

Blog de ingenieria: https://blog.tango.io/

Tecnologias:
Adobe
Welldrive
Orbridge




7


# Unosquare

Link: https://www.unosquare.com/

Ubicación: https://goo.gl/maps/fK8U375eJzuvTJg3A

Acerca de: Unosquare, en su esencia, tiene una misión impulsada por tres propósitos rectores para cada individuo que forma parte de la organización:

PERSONAL
Básicamente, la empresa de desarrollo de software ágil se creó entendiendo que, en nuestro núcleo, no somos más que un grupo de personas que empujan nuestros límites personales para hacer el mejor trabajo que hemos hecho. Un buen ambiente de trabajo es clave para la creatividad, la colaboración y, en última instancia, el éxito.

PROFESIONAL
Nos esforzamos por crear un entorno donde nuestro personal pueda disfrutar de la base cultural y las relaciones personales que han creado a lo largo de su tiempo aquí. También queremos colaborar en proyectos que desafíen nuestras capacidades técnicas y que tengan un nivel de complejidad que impulse el crecimiento profesional del individuo y del grupo, el emprendimiento y el comportamiento auto didáctico.

SOCIAL
Unosquare está aquí para crear un legado. Tenemos el compromiso de generar bienestar en todas las geografías en las que tenemos presencia. Nuestra organización ha sido bendecida con la oportunidad de impactar sustancialmente el espíritu en el que continuamos desarrollando la empresa. Creemos en un esfuerzo global hacia la igualdad de oportunidades.

Servicios:
DESARROLLO ÁGIL DE SOFTWARE
SERVICIOS DE DESARROLLO NEARSHORE
CONSULTORÍA DE PROYECTOS DE TECNOLOGÍA
ESTRATEGIAS DE TRANSFORMACIÓN DIGITAL

Presencia:
https://www.facebook.com/unosquare/
https://twitter.com/unosquare

Ofertas laborales: https://people.unosquare.com/#testimonies

Blog de ingenieria: https://blog.unosquare.com/

Tecnologias:
Cureone
Hubbub
Madcap




8


# Grow IT

Link: http://www.grw.com.mx/

Ubicación: https://goo.gl/maps/nXkDfb2VwH7TGWbP7

Servicios: 
Dynamics SL
POS

Presencia:
https://www.facebook.com/growitc
https://www.instagram.com/grow_it_mx/

Ofertas laborales: http://www.grw.com.mx/bolsa-de-trabajo/

Blog de ingenieria: http://www.grw.com.mx/blog/





9


# OneCard

Link: https://onecard.mx/

Ubicación: https://goo.gl/maps/CjNMwJMw56BdrGEt6

Acerca de: Somos una empresa con experiencia en la emisión y administración de tarjetas de prepago.

Servicios:
La plataforma inteligente
Tarjeta empresarial para
gastos, víaticos y caja chica

Presencia:
https://www.instagram.com/onecardsi/
https://twitter.com/onecardsi
https://www.facebook.com/onecardsi/






10


# OnePhase

Link: https://www.onephase.com/

Ubicación: https://goo.gl/maps/QmQivNpkwRxBTXai9

Acerca de: Somos una empresa de Consultoría Digital y Desarrollo de Software a la medida, entregamos soluciones a la medida mediante nuestro enfoque Centrado en el Cliente. En Onephase ayudamos a los negocios a realizar una transición exitosa a lo Digital.

Servicios:
Consultoría Digital
Desarrollo de software
Control de calidad y pruebas de software
Comercio electrónico
Outsourcing de recursos
UI / UX
Servicios de RV/RA
Servicios en la nube
Big Data

Presencia:
https://www.facebook.com/OnephaseLLC/
https://www.instagram.com/onephasellc/
https://twitter.com/OnePhaseLLC

Tecnologias:
NodeJS
Iónico
Angular
SQL
AWS




11


# Roomie IT

Link: https://www.roomie-it.org/

Ubicación: Dirección: Calz. Gral. Mariano Escobedo 526-Piso 8, Anzures, Miguel Hidalgo, 11590 Ciudad de México, CDMX

Acerca de: Somos la primera empresa latinoamericana de robótica que busca transformar organizaciones a través de robots de servicio.

Servicios:
Robótica
Datos e IA
Soluciones nativas en la nube

Presencia: 
https://www.facebook.com/roomieit/
https://www.instagram.com/explore/locations/103686894470554/roomie-it
https://twitter.com/IT_ROOMIE
https://www.youtube.com/channel/UCSTsOKChfqE948Sd7LwX5WQ


Ofertas laborales: https://www.roomie-it.org/careers

Blog de ingenieria: https://www.roomie-it.org/blog

Tecnologias:
Walmart
Banorte
Eleven
Profuturo
Sura




12


# Tacit Knowledge

Link: https://www.tacitknowledge.com/

Ubicación: 

Acerca de: Somos solucionadores de problemas. Desde nuestra fundación en 2002, nunca nos hemos encontrado con un problema que no hayamos superado. Escuchamos atentamente a nuestros clientes y aprovechamos nuestra experiencia colectiva para ofrecer resultados óptimos en un amplio espectro de desafíos comerciales y tecnológicos. Nuestro espíritu es ganarnos la confianza, construir asociaciones duraderas y ayudar a nuestros clientes a hacer crecer su negocio. Nos enfocamos en crear valor para nuestros clientes ayudándolos a satisfacer y deleitar constantemente a los compradores en todos los canales y dispositivos. Desde el desarrollo y la integración de software empresarial hasta la optimización y el soporte máximos, abordamos todo el ecosistema de la propiedad de comercio digital de cada cliente para ayudar a las marcas a superar a la competencia.

Servicios: 
CONSULTORÍA DE SOFTWARE
CONSULTORÍA ECOMMERCE
COMERCIO DIGITAL
RENDIMIENTO MÁXIMO LISTO
SERVICIOS GESTIONADOS
COMERCIO SAP
COMUNIDAD DE CÓDIGO ABIERTO
NCOMMERCE

Presencia: 
https://twitter.com/tacitknowledge

Ofertas laborales: https://www.tacitknowledge.com/careers

Tecnologias:
Adobe
Fastly
New relic
Amplience




13


# Intelimétrica

Link: https://www.intelimetrica.com/

Ubicación: https://goo.gl/maps/Xpd2cFpcRtdd44Jy8

Acerca de: Somos especialistas en impulsar la transformación digital de organizaciones con enfoque en datos, análisis y diseño de usuarios.

Servicios: 
IA y ML
Ingeniería de software
Diseño UX

Presencia:
https://twitter.com/Intelimetrica
https://www.facebook.com/intelimetrica

Ofertas laborales: https://www.intelimetrica.com/careers

Blog de ingenieria: https://github.com/Intelimetrica



14


# Kata Software

Link: https://kata-software.com/es/homeA

Ubicación: https://goo.gl/maps/iQjecfXBdv5MToyz8

Acerca de: Kata Software es la plataforma SaaS más robusta con soluciones digitales para instituciones financieras.
Ayudamos a bancos y microfinancieras a dar el paso hacia la era digital; construyendo soluciones que mejoran la gestión de procesos y responden a las necesidades tecnológicas del mercado financiero.

Presencia:
https://www.facebook.com/Kata-Software-109450663980021/

Ofertas laborales: https://kata-software.com/es/trabaja-con-nosotros

Tecnologias:
Financiela independencia
Afirme
Yastás




15


# Telepro

Link: https://www.telepro.com.mx/index.html

Ubicación: https://goo.gl/maps/LnJGY2nryd5pVaLv5

Acerca de: Telepro se enfoca en la excelencia en el servicio para proveer valor agregado a nuestros clientes.

Presencia:
https://www.facebook.com/InfoTelepro

Tecnologias:
Toyota
BBVA
Daimler




16


# Grupo GFT

Link: https://www.gft.com/mx/es/index/

Ubicación: https://goo.gl/maps/upG1tUANFfnR86Uo8

Acerca de: A la vanguardia del cambio desde 1987, combinando tecnología y servicios financieros.

Servicios:
Inteligencia artificial
Analisis de datos
Cloud

Presencia:
https://www.youtube.com/user/gftgroup
https://www.instagram.com/gft_tech/
https://twitter.com/gft_mx
https://www.facebook.com/gft.mex

Ofertas laborales: https://jobs.gft.com/Mexico/go/mexico/4412401/

Blog de ingenieria: https://blog.gft.com/es/





